package zap.graph;

import net.minecraft.util.math.Direction;

public interface IConnectable {
	boolean connects(Direction direction);
}
